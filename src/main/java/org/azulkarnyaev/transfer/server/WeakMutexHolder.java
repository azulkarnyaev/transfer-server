package org.azulkarnyaev.transfer.server;

import com.google.common.collect.MapMaker;

import java.util.concurrent.ConcurrentMap;

/**
 * Provide id based lock mechanism.
 * <p>
 * Hold currently used mutexes and return them if need.
 * Old unused mutexes will be deleted after GC invocation.
 * <p>
 * Note that several instances of {@link WeakMutexHolder} can't be
 * provide thread safe mechanism for id-based locks.
 *
 * @param <K> mutex key type
 */
public class WeakMutexHolder<K> {

    private final ConcurrentMap<K, Object> weakConcurrentMutexStorage = new MapMaker()
            .weakValues()
            .makeMap();

    /**
     * Return mutex instance by the key
     * <p>
     * If mutex is already used then returns the same instance of the mutex.
     * Otherwise method can return a new instance or old value for that key.
     * @param key not null
     */
    public Object getWeakMutex(K key) {
        if (key == null) {
            throw new IllegalArgumentException("Mutex key can't be null");
        }
        return weakConcurrentMutexStorage.computeIfAbsent(key, k -> new Object());
    }

}
