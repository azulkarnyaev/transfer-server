package org.azulkarnyaev.transfer.server;

import com.google.gson.Gson;
import org.azulkarnyaev.transfer.server.account.*;
import org.azulkarnyaev.transfer.server.transfer.*;

import static spark.Spark.*;

public class App {

    public static void main(String[] args) {

        AccountDao accountDao = new InMemoryAccountDao();
        TransferDao transferDao = new InMemoryTransferDao();
        AccountService accountService = new AccountService(accountDao, transferDao);

        Gson gson = new Gson();

        port(Integer.parseInt(System.getProperty("port", "4567")));

        post("/account",
                (request, response) -> {
                    AccountCreationResult accountCreationResult = accountService.createAccount(gson.fromJson(request.body(),
                            AccountInputParameters.class));
                    AccountCreationResult.Status status = accountCreationResult.getStatus();
                    if (status == AccountCreationResult.Status.ERROR) {
                        response.status(422);
                    }
                    return accountCreationResult;
                },
                gson::toJson
        );

        get("/account/:id",
                (req, res) -> {
                    int id;
                    try {
                        id = Integer.parseInt(req.params(":id"));
                    } catch (NumberFormatException e) {
                        res.status(400);
                        return "Illegal account id";
                    }
                    return accountService.getAccount(id)
                            .map(gson::toJson)
                            .orElseGet(() -> {
                                res.status(404);
                                return "Account not found";
                            });
                }
        );

        delete("/account/:id",
                (request, response) -> accountService.deleteAccount(Integer.parseInt(request.params(":id")))
                        .map(gson::toJson)
                        .orElseGet(() -> {
                            response.status(404);
                            return "Account not found";
                        })
        );

        post("/transfer",
                (request, response) -> {
                    Transfer transfer = accountService.transfer(gson.fromJson(request.body(), TransferInputParameter.class));
                    TransferResult.Result result = transfer.getTransferResult().getResult();
                    if (TransferResult.Result.SUCCESS != result) {
                        response.status(422);
                    }
                    return transfer;
                },
                gson::toJson
        );

        get("/transfer/:id",
                (request, response) -> transferDao.getTransfer(Integer.parseInt(request.params(":id")))
                        .map(gson::toJson)
                        .orElseGet(() -> {
                            response.status(404);
                            return "Transfer not found";
                        })
        );

    }

}
