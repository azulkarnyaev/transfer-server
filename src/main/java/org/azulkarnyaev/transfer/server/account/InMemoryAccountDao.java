package org.azulkarnyaev.transfer.server.account;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

public class InMemoryAccountDao implements AccountDao {

    private static AtomicLong idGenerator = new AtomicLong();
    private final ConcurrentMap<Long, Account> accountsStorage = new ConcurrentHashMap<>();

    @Override
    public Account createAccount(AccountInputParameters account) {
        long newId = idGenerator.getAndIncrement();
        Account newAccount = new Account(newId, account.getBalance(), account.getUserName());
        accountsStorage.put(newId, newAccount);
        return newAccount;
    }

    @Override
    public Optional<Account> deleteAccount(long id) {
        return Optional.ofNullable(accountsStorage.remove(id));
    }

    @Override
    public Optional<Account> getAccount(long id) {
        return Optional.ofNullable(accountsStorage.get(id));
    }

    @Override
    public void updateAccount(Account account) {
        accountsStorage.put(account.getId(), account);
    }

}
