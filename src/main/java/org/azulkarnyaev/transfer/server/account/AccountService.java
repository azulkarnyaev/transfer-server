package org.azulkarnyaev.transfer.server.account;

import org.azulkarnyaev.transfer.server.WeakMutexHolder;
import org.azulkarnyaev.transfer.server.transfer.Transfer;
import org.azulkarnyaev.transfer.server.transfer.TransferDao;
import org.azulkarnyaev.transfer.server.transfer.TransferInputParameter;
import org.azulkarnyaev.transfer.server.transfer.TransferResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Optional;

public class AccountService {

    private final static Logger logger = LoggerFactory.getLogger(AccountService.class);

    private final AccountDao accountDao;
    private final TransferDao transferDao;
    private final WeakMutexHolder<Long> weakMutexHolder = new WeakMutexHolder<>();

    public AccountService(AccountDao accountDao, TransferDao transferDao) {
        this.accountDao = accountDao;
        this.transferDao = transferDao;
    }

    public AccountCreationResult createAccount(AccountInputParameters accountInput) {
        if (accountInput.getUserName() == null || accountInput.getUserName().isEmpty()) {
            return new AccountCreationResult("Empty user name");
        }
        if (accountInput.getBalance() == null) {
            accountInput = new AccountInputParameters(new BigDecimal(0), accountInput.getUserName());
        }
        return new AccountCreationResult(accountDao.createAccount(accountInput));
    }

    public Optional<Account> getAccount(long accountId) {
        synchronized (weakMutexHolder.getWeakMutex(accountId)) {
            return accountDao.getAccount(accountId);
        }
    }

    public Optional<Account> deleteAccount(long accountId) {
        synchronized (weakMutexHolder.getWeakMutex(accountId)) {
            return accountDao.deleteAccount(accountId);
        }
    }

    public Transfer transfer(TransferInputParameter transferInputParams) {
        TransferResult transferResult;
        if (transferInputParams.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
            transferResult = new TransferResult(TransferResult.Result.ERROR, "Transfer amount must be greater than 0");
        } else {
            Object innerMutex;
            Object outerMutex;

            // We need ordering rule to avoid deadlocks
            if (transferInputParams.getSourceAccount() > transferInputParams.getTargetAccount()) {
                innerMutex = weakMutexHolder.getWeakMutex(transferInputParams.getTargetAccount());
                outerMutex = weakMutexHolder.getWeakMutex(transferInputParams.getSourceAccount());
            } else {
                innerMutex = weakMutexHolder.getWeakMutex(transferInputParams.getSourceAccount());
                outerMutex = weakMutexHolder.getWeakMutex(transferInputParams.getTargetAccount());
            }

            synchronized (outerMutex) {
                synchronized (innerMutex) {
                    transferResult = syncTransfer(transferInputParams);
                }
            }
        }
        return transferDao.createTransfer(transferInputParams, transferResult);
    }

    private TransferResult syncTransfer(TransferInputParameter transfer) {
        Optional<Account> sourceAccountOpt = accountDao.getAccount(transfer.getSourceAccount());
        Optional<Account> targetAccountOpt = accountDao.getAccount(transfer.getTargetAccount());

        if (!sourceAccountOpt.isPresent()) {
            return new TransferResult(TransferResult.Result.ERROR, "Can't find source account");
        }
        if (!targetAccountOpt.isPresent()) {
            return new TransferResult(TransferResult.Result.ERROR, "Can't find target account");
        }

        Account source = sourceAccountOpt.get();
        Account target = targetAccountOpt.get();

        if (!source.hasMoney(transfer.getAmount())) {
            return new TransferResult(TransferResult.Result.NOT_ENOUGH_MONEY, "Not enough money in the source account");
        }
        Account updatedSource = source.withdraw(transfer.getAmount());
        Account updatedTarget = target.deposit(transfer.getAmount());

        try {
            accountDao.updateAccount(updatedSource);
            accountDao.updateAccount(updatedTarget);
        } catch (Exception e) {
            logger.error("User accounts updating error. Transfer will be canceled", e);
            accountDao.updateAccount(source);
            accountDao.updateAccount(target);
            return new TransferResult(TransferResult.Result.ERROR, e.getMessage());
        }
        return new TransferResult(TransferResult.Result.SUCCESS, null);
    }

}
