package org.azulkarnyaev.transfer.server.account;

import java.math.BigDecimal;

public class AccountInputParameters {

    private final BigDecimal balance;
    private final String userName;

    public AccountInputParameters(BigDecimal balance, String userName) {
        this.balance = balance;
        this.userName = userName;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getUserName() {
        return userName;
    }
}
