package org.azulkarnyaev.transfer.server.account;

import java.util.Optional;

public interface AccountDao {

    Account createAccount(AccountInputParameters account);
    Optional<Account> deleteAccount(long id);
    Optional<Account> getAccount(long id);
    void updateAccount(Account account);

}
