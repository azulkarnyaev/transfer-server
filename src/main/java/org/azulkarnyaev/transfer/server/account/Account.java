package org.azulkarnyaev.transfer.server.account;

import java.math.BigDecimal;
import java.util.Objects;

public class Account {

    private final long id;
    private final BigDecimal balance;
    private final String userName;

    public Account(long id, BigDecimal balance, String userName) {
        this.id = id;
        this.balance = balance;
        this.userName = userName;
    }

    public long getId() {
        return id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getUserName() {
        return userName;
    }

    public Account withdraw(BigDecimal amount) {
        return new Account(id, balance.subtract(amount), userName);
    }

    public Account deposit(BigDecimal amount) {
        return new Account(id, balance.add(amount), userName);
    }

    public boolean hasMoney(BigDecimal amount) {
        return balance.compareTo(amount) >= 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id &&
                Objects.equals(balance, account.balance) &&
                Objects.equals(userName, account.userName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, balance, userName);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", balance=" + balance +
                ", userName='" + userName + '\'' +
                '}';
    }

}
