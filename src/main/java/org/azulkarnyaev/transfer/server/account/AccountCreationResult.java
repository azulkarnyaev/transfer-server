package org.azulkarnyaev.transfer.server.account;

import javax.annotation.Nullable;

public class AccountCreationResult {

    public enum Status {
        SUCCESS,
        ERROR
    }

    private final Status status;
    @Nullable
    private final String message;
    private final Account account;

    public AccountCreationResult(Account account) {
        this(account, Status.SUCCESS, null);
    }

    public AccountCreationResult(String error) {
        this(null, Status.ERROR, error);
    }

    public AccountCreationResult(Account account, Status status, @Nullable String message) {
        this.account = account;
        this.status = status;
        this.message = message;
    }

    public Status getStatus() {
        return status;
    }

    @Nullable
    public String getMessage() {
        return message;
    }

}
