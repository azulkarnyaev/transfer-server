package org.azulkarnyaev.transfer.server.transfer;

import java.util.Optional;

public interface TransferDao {

    Transfer createTransfer(TransferInputParameter transfer, TransferResult result);
    Optional<Transfer> getTransfer(long id);

}
