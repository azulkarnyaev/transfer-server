package org.azulkarnyaev.transfer.server.transfer;

import java.math.BigDecimal;

public class Transfer {

    private final Long id;
    private final long sourceAccount;
    private final long targetAccount;
    private final BigDecimal amount;
    private final TransferResult transferResult;

    public Transfer(Long id,
                    long sourceAccount,
                    long targetAccount,
                    BigDecimal amount,
                    TransferResult transferResult) {
        this.id = id;
        this.sourceAccount = sourceAccount;
        this.targetAccount = targetAccount;
        this.amount = amount;
        this.transferResult = transferResult;
    }

    public Long getId() {
        return id;
    }

    public long getSourceAccount() {
        return sourceAccount;
    }

    public long getTargetAccount() {
        return targetAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public TransferResult getTransferResult() {
        return transferResult;
    }

    @Override
    public String toString() {
        return "Transfer{" +
                "id=" + id +
                ", sourceAccount=" + sourceAccount +
                ", targetAccount=" + targetAccount +
                ", amount=" + amount +
                ", transferResult=" + transferResult +
                '}';
    }

}
