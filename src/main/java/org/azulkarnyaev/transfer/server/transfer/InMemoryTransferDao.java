package org.azulkarnyaev.transfer.server.transfer;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

public class InMemoryTransferDao implements TransferDao {

    private final AtomicLong idGenerator = new AtomicLong();
    private final ConcurrentMap<Long, Transfer> transferStorage = new ConcurrentHashMap<>();

    @Override
    public Transfer createTransfer(TransferInputParameter transferInputParameter, TransferResult result) {
        long newId = idGenerator.getAndIncrement();
        Transfer transfer = new Transfer(newId, transferInputParameter.getSourceAccount(),
                transferInputParameter.getTargetAccount(), transferInputParameter.getAmount(), result);
        transferStorage.put(newId, transfer);
        return transfer;
    }

    @Override
    public Optional<Transfer> getTransfer(long id) {
        return Optional.ofNullable(transferStorage.get(id));
    }

}
