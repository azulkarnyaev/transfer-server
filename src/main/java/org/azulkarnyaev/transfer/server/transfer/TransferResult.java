package org.azulkarnyaev.transfer.server.transfer;

import javax.annotation.Nullable;

public class TransferResult {

    public enum Result {
        SUCCESS, ERROR, NOT_ENOUGH_MONEY
    }

    private final Result result;

    @Nullable
    private final String message;

    public TransferResult(Result result, @Nullable String message) {
        this.result = result;
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    @Nullable
    public String getMessage() {
        return message;
    }
}
