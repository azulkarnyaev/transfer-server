package org.azulkarnyaev.transfer.server.transfer;

import java.math.BigDecimal;

public class TransferInputParameter {

    private final long sourceAccount;
    private final long targetAccount;
    private final BigDecimal amount;

    public TransferInputParameter(long sourceAccount, long targetAccount, BigDecimal amount) {
        this.sourceAccount = sourceAccount;
        this.targetAccount = targetAccount;
        this.amount = amount;
    }

    public long getSourceAccount() {
        return sourceAccount;
    }

    public long getTargetAccount() {
        return targetAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "TransferInputParameter{" +
                "sourceAccount=" + sourceAccount +
                ", targetAccount=" + targetAccount +
                ", amount=" + amount +
                '}';
    }
}
