package org.azulkarnyaev.transfer.server.account;

import org.azulkarnyaev.transfer.server.transfer.Transfer;
import org.azulkarnyaev.transfer.server.transfer.TransferDao;
import org.azulkarnyaev.transfer.server.transfer.TransferInputParameter;
import org.azulkarnyaev.transfer.server.transfer.TransferResult;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class AccountServiceTest {

    private AccountService accountService;

    @Mock
    private AccountDao accountDao;

    @Mock
    private TransferDao transferDao;

    @Before
    public void init() {
        final AtomicLong idIterator = new AtomicLong();
        MockitoAnnotations.initMocks(this);
        when(transferDao.createTransfer(any(TransferInputParameter.class), any(TransferResult.class)))
                .then(invocation -> {
                            TransferInputParameter inputParameter = invocation.getArgument(0);
                            TransferResult transferResult = invocation.getArgument(1);
                            return new Transfer(idIterator.getAndIncrement(), inputParameter.getSourceAccount(),
                                    inputParameter.getTargetAccount(), inputParameter.getAmount(), transferResult);
                        }
                );
        accountService = new AccountService(accountDao, transferDao);
    }

    @Test
    public void transfer_transferMoneyFromSourceToTarget() {
        final long sourceAccId = 1;
        final long targetAccId = 2;

        TransferInputParameter transferInputParameter = new TransferInputParameter(sourceAccId, targetAccId,
                new BigDecimal(100));

        when(accountDao.getAccount(sourceAccId)).thenReturn(Optional.of(new Account(1L, new BigDecimal(200),
                "Tommy")));
        when(accountDao.getAccount(targetAccId)).thenReturn(Optional.of(new Account(2L, new BigDecimal(300),
                "Boris Blade")));

        Transfer transfer = accountService.transfer(transferInputParameter);
        verifyTransfer(TransferResult.Result.SUCCESS, transferInputParameter, transfer);

        ArgumentCaptor<Account> accountCaptor = ArgumentCaptor.forClass(Account.class);
        verify(accountDao, times(2)).updateAccount(accountCaptor.capture());
        List<Account> updatedAccounts = accountCaptor.getAllValues();

        Account newUpdatedSource = updatedAccounts.get(0);
        Account newUpdatedTarget = updatedAccounts.get(1);

        assertThat(newUpdatedSource.getBalance()).isEqualTo(new BigDecimal(100).toString());
        assertThat(newUpdatedTarget.getBalance()).isEqualTo(new BigDecimal(400).toString());
    }

    @Test
    public void transfer_notEnoughMoneyInSourceAccount() {
        final long sourceAccId = 3;
        final long targetAccId = 7;

        TransferInputParameter transferInputParameter = new TransferInputParameter(sourceAccId, targetAccId,
                new BigDecimal(1000));

        when(accountDao.getAccount(sourceAccId)).thenReturn(Optional.of(new Account(sourceAccId, new BigDecimal(500),
                "Turkish")));
        when(accountDao.getAccount(targetAccId)).thenReturn(Optional.of(new Account(targetAccId, new BigDecimal(100000),
                "Brick Top")));

        Transfer transfer = accountService.transfer(transferInputParameter);
        verifyTransfer(TransferResult.Result.NOT_ENOUGH_MONEY, transferInputParameter, transfer);

        verify(accountDao, never()).updateAccount(any(Account.class));
    }

    @Test
    public void transfer_canNotFindSourceAccount() {
        final long sourceAccId = 101;
        final long targetAccId = 1;

        TransferInputParameter transferInputParameter = new TransferInputParameter(sourceAccId, targetAccId,
                new BigDecimal(300));

        when(accountDao.getAccount(targetAccId)).thenReturn(Optional.of(new Account(targetAccId, new BigDecimal(500),
                "Tommy")));

        Transfer transfer = accountService.transfer(transferInputParameter);
        verifyTransfer(TransferResult.Result.ERROR, transferInputParameter, transfer);

        verify(accountDao, never()).updateAccount(any(Account.class));
    }

    @Test
    public void transfer_canNotFindTargetAccount() {
        final long sourceAccId = 1;
        final long targetAccId = 101;

        TransferInputParameter transferInputParameter = new TransferInputParameter(sourceAccId, targetAccId,
                new BigDecimal(300));

        when(accountDao.getAccount(sourceAccId)).thenReturn(Optional.of(new Account(sourceAccId, new BigDecimal(500),
                "Tommy")));

        Transfer transfer = accountService.transfer(transferInputParameter);
        verifyTransfer(TransferResult.Result.ERROR, transferInputParameter, transfer);

        verify(accountDao, never()).updateAccount(any(Account.class));

    }

    @Test
    public void transfer_rollbackWhenExceptionOccurs() {
        final long sourceAccId = 10;
        final long targetAccId = 7;

        TransferInputParameter transferInputParameter = new TransferInputParameter(sourceAccId, targetAccId,
                new BigDecimal(300));

        when(accountDao.getAccount(sourceAccId)).thenReturn(Optional.of(new Account(sourceAccId, new BigDecimal(500),
                "Mickey O'Neil")));
        when(accountDao.getAccount(targetAccId)).thenReturn(Optional.of(new Account(targetAccId, new BigDecimal(100000),
                "Brick Top")));

        doNothing()
                .doThrow(new RuntimeException("Something wrong"))
                .doNothing()
                .when(accountDao).updateAccount(any(Account.class));

        Transfer transfer = accountService.transfer(transferInputParameter);
        verifyTransfer(TransferResult.Result.ERROR, transferInputParameter, transfer);

        ArgumentCaptor<Account> accountCaptor = ArgumentCaptor.forClass(Account.class);
        verify(accountDao, times(4)).updateAccount(accountCaptor.capture());

        List<Account> updatedAccs = accountCaptor.getAllValues();
        Account sourceSuccessUpdate = updatedAccs.get(0);

        Account rollbackSource = updatedAccs.get(2);
        Account rollbackTarget = updatedAccs.get(3);

        assertThat(sourceSuccessUpdate.getBalance()).isEqualTo(new BigDecimal(200));
        assertThat(rollbackSource.getBalance()).isEqualTo(new BigDecimal(500));
        assertThat(rollbackTarget.getBalance()).isEqualTo(new BigDecimal(100000));
    }

    @Test
    public void transfer_transferNegativeSum_returnError() {
        final long sourceAccId = 10;
        final long targetAccId = 7;

        TransferInputParameter transferInputParameter = new TransferInputParameter(sourceAccId, targetAccId,
                new BigDecimal(-300));

        when(accountDao.getAccount(sourceAccId)).thenReturn(Optional.of(new Account(sourceAccId, new BigDecimal(100),
                "Mickey O'Neil")));
        when(accountDao.getAccount(targetAccId)).thenReturn(Optional.of(new Account(targetAccId, new BigDecimal(100000),
                "Brick Top")));

        Transfer transfer = accountService.transfer(transferInputParameter);
        verifyTransfer(TransferResult.Result.ERROR, transferInputParameter, transfer);

        verify(accountDao, never()).updateAccount(any(Account.class));
    }

    private void verifyTransfer(TransferResult.Result expectedResult,
                                TransferInputParameter transferInputParameter,
                                Transfer transfer) {
        assertThat(transfer.getTransferResult().getResult()).isEqualTo(expectedResult);
        assertThat(transfer.getAmount()).isEqualTo(transferInputParameter.getAmount().toString());
        assertThat(transfer.getSourceAccount()).isEqualTo(transferInputParameter.getSourceAccount());
        assertThat(transfer.getTargetAccount()).isEqualTo(transferInputParameter.getTargetAccount());
    }

}