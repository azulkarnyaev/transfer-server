package org.azulkarnyaev.transfer.server;

import com.google.common.collect.MapMaker;
import org.junit.Ignore;
import org.junit.Test;

import java.util.concurrent.ConcurrentMap;

import static org.assertj.core.api.Assertions.assertThat;

public class WeakMutexHolderTest {

    @Test
    public void obtainMutex_getSameMutexForSameKey() {
        WeakMutexHolder<Long> weakMutexHolder = new WeakMutexHolder<>();
        Object mutex1 = weakMutexHolder.getWeakMutex(100L);
        Object mutex2 = weakMutexHolder.getWeakMutex(100L);
        Object mutex3 = weakMutexHolder.getWeakMutex(new Long(100));

        assertThat(mutex1).isNotNull();
        assertThat(mutex1).isSameAs(mutex2);
        assertThat(mutex1).isSameAs(mutex3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void obtainMutex_nullValue() {
        new WeakMutexHolder<>().getWeakMutex(null);
    }

    @Test
    public void obtainMutex_getDifferentMutexForDifferentKeys() {
        WeakMutexHolder<Integer> weakMutexHolder = new WeakMutexHolder<>();

        Object mutex1 = weakMutexHolder.getWeakMutex(100);
        Object mutex2 = weakMutexHolder.getWeakMutex(101);

        assertThat(mutex1).isNotNull();
        assertThat(mutex2).isNotNull();
        assertThat(mutex1).isNotEqualTo(mutex2);
    }

}